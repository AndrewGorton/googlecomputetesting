#!/bin/bash

if [ "$#" -lt 1 ]; then
    echo "Incorrect number of parameters"
    echo ""
    echo "USAGE: $(basename $0) <project_id>"
    echo ""
    echo "Removes all running VM instances in the specified project"
else
  gcloud --project $1 compute instances list | tail +2 | awk '{print $1,"--zone",$2;}' | xargs -L 1 gcloud --project=$1 compute instances delete -q
fi
