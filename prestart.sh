#!/bin/bash

# Set the discovery token
cp etcd.yml.template etcd.yml
curl https://discovery.etcd.io/new > discovery.token
sed -i '' -e "s,#discovery: https://discovery.etcd.io/<token>,discovery: $(sed 's:/:\\/:g' discovery.token),"  etcd.yml

# Create the ETCD startup script
cat << EOF > start_etcd.sh
#!/bin/bash

ZONE=europe-west1-a
MACHINE=f1-micro
IMAGE="https://www.googleapis.com/compute/v1/projects/coreos-cloud/global/images/coreos-alpha-457-0-0-v20141002"

if [ "\$#" -lt 2 ]; then
  echo "Incorrect number of parameters"
  echo ""
  echo "USAGE: \$(basename \$0) <project_id> <instance_name> [<instance_name> ...]"
  echo ""
  echo "Creates clustered ETCD VM instances under the given project."
else
  PROJECT=\$1
  shift
  echo gcloud --project \$PROJECT \\
    compute instances create \$@ \\
    --image \$IMAGE \\
    --zone \$ZONE \\
    --machine-type \$MACHINE \\
    --metadata-from-file user-data=etcd.yml \\
    --metadata role=etcd
  gcloud --project \$PROJECT \\
    compute instances create \$@ \\
    --image \$IMAGE \\
    --zone \$ZONE \\
    --machine-type \$MACHINE \\
    --metadata-from-file user-data=etcd.yml \\
    --metadata role=etcd

    echo To connect to your machine, try
    echo    gcloud compute --project \$PROJECT ssh --zone \$ZONE \$1
    echo and then to list the machines in the cluster, use
    echo    fleetctl list-machines
fi
EOF
chmod 744 start_etcd.sh

echo Add more machines to the ETCD cluster with './start_etcd.sh <project_id> <instance_name> [<instance_name> ...]'
