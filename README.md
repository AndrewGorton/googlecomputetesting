# GoogleComputeTesting

Initial repository for playing around with [Google Compute Engine](https://cloud.google.com/).

Currently, this enables a quick spin up of an ETCD cluster and enables you to add additional nodes.

## Running

You need the [gcloud command line utility](https://cloud.google.com/sdk/) installed, and a Google Cloud account with billing enabled.

```bash
./prestart.sh
```

creates a start_etcd.sh script which can create machines in a cluster. Re-run this every time you tear down the cluster to gain a new [discovery token](https://coreos.com/docs/cluster-management/setup/cluster-discovery/) (otherwise your fresh nodes will try to connect to non-existant peers).

```bash
./start_etcd.sh <project_id> <nodename> [<nodename> ...]
```

This starts up a new VM (or set of VMs) using the pre-acquired discovery token. The nodes will either create a new cluster if no cluster exists, or joins the existing cluster.


## Stopping
I've provided a script to terminate the instances in a given project.

```bash
./quickstop.sh <project_name>
```

Be warned - this terminates all running VM instances, so if you have additional VMs you're using for something, then don't use this!
